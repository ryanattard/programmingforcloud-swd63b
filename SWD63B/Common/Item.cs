﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Item
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Id { get; set; }
        public int Category_Fk { get; set; }
        public string ImagePath { get; set; }



        public Category ItemCategory { get; set; }

    }
}
