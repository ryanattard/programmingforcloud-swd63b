﻿using Google.Api;
using Google.Cloud.Logging.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProgrammingForTheCloud63B_Website.Models
{
    public class Logger
    {
        public static void Log(string message, Google.Cloud.Logging.Type.LogSeverity severity)
        {
            var logger = LoggingServiceV2Client.Create();
            string projectId = "progforcloud63at";
            string logId = "myLog";
            LogName logName = new LogName(projectId, logId);

            LogEntry entry = new LogEntry();
            entry.LogName = logName.ToString();
            entry.Severity = severity;
            entry.TextPayload = message;


            MonitoredResource resource = new MonitoredResource();
            resource.Type = "global";

            logger.WriteLogEntries(
                LogNameOneof.From(logName),
                resource,
                null,
                new List<LogEntry>() { entry }
                );

        }
    }
}