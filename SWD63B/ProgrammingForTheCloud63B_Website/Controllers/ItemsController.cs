﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using DataAccess;
using Npgsql;
using Google.Cloud.Storage.V1;
using System.IO;
using Google.Cloud.Diagnostics.AspNet;
using ProgrammingForTheCloud63B_Website.Models;

namespace ProgrammingForTheCloud63B_Website.Controllers
{
    public class ItemsController : Controller
    {
        // GET: Items
      //  [Authorize]
        public ActionResult Index()
        {
            ItemsRepository ir = new ItemsRepository();
            List<Item> items = new List<Item>();
            try
            {
                Logger.Log("loading items from cache....", Google.Cloud.Logging.Type.LogSeverity.Info);
                items = new RedisRepository().LoadItemsFromCache();
            }
            catch (Exception ex)
            {
                Logger.Log("error occurred while loading errors from cache", Google.Cloud.Logging.Type.LogSeverity.Error);
                //log exception
                var logger = GoogleExceptionLogger.Create("progforcloud63at", "SWD63BPFTC", "1");
                logger.Log(ex);
                ViewBag.Error = "Error occurred. try again later";
            }
            finally
            {
                  
            }
            return View(items);
        }



        public ActionResult SearchByCategory(int category)
        {
            ItemsRepository ir = new ItemsRepository();
            List<Item> items = new List<Item>();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                items = ir.GetItems(category).ToList();
            }
            catch (Exception ex)
            {
                //log exception
                ViewBag.Error = "Error occurred. try again later";
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
            return View("Index",items);
        }


        [HttpGet]
        public  ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Item i, HttpPostedFileBase file)
        {
            ItemsRepository ir = new ItemsRepository();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                //need to upload the file somewhere on the cloud storage

                var client = StorageClient.Create();
                var imageName = Guid.NewGuid() +  Path.GetExtension(file.FileName);
                var uploadObject =  client.UploadObject("progforcloud63bt", imageName.ToString(), file.ContentType, file.InputStream,
                   new UploadObjectOptions() { PredefinedAcl = PredefinedObjectAcl.ProjectPrivate });
                
                i.ImagePath = imageName; //uploadObject.MediaLink
                ir.AddItem(i);

                ViewBag.Success = "Item was added";
            }
            catch (Exception ex)
            {
                //log exception
                ViewBag.Error = "Error occurred. try again later";
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
            return View();
        }


        public ActionResult DownloadItem(string id)
        {

            //authorize the request
            //check whether the current logged in user has permissions to download the requested file

            var client = StorageClient.Create();
            MemoryStream msOut = new MemoryStream();
            client.DownloadObject("progforcloud63bt", id, msOut);

            return File(msOut.ToArray(), System.Net.Mime.MediaTypeNames.Application.Octet, id);

        }



        public ActionResult DeleteItems(List<int> items)
        {
            if(items.Count > 0)
            {
                
                ItemsRepository ir = new ItemsRepository();
                List<Item> myList = new List<Item>();
                try
                {
                    if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    {
                        ir.MyConnection.Open();
                        ir.MyTransaction = ir.MyConnection.BeginTransaction();

                    }
                    
                    //delete all items
                    foreach(int id in items)
                    {
                        ir.DeleteItem(id);
                    }

                    ir.MyTransaction.Commit();

                    if(items.Count==1)
                         ViewBag.Success = "Item was deleted";
                    else
                        ViewBag.Success = "Items were deleted";
                }
                catch (Exception ex)
                {
                    ir.MyTransaction.Rollback();

                    //log exception
                    ViewBag.Error = "Error occurred. Nothing was deleted; Try again later";
                }
                finally
                {
                    myList = ir.GetItems().ToList(); //to refresh the now updated list

                    if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                        ir.MyConnection.Close();
                }

                return View("Index", myList);

            }
            
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            ItemsRepository ir = new ItemsRepository();
            Item i = new Item();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                i = ir.GetItem(id);
            }
            catch (Exception ex)
            {
                //log exception
                ViewBag.Error = "Error occurred. try again later";
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
            return View(i);
        }

        [HttpPost]
        public ActionResult Edit(Item current)
        {
            ItemsRepository ir = new ItemsRepository();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                  ir.UpdateItem(current);

                ViewBag.Success = "Item updated!";
            }
            catch (Exception ex)
            {
                //log exception
                ViewBag.Error = "Error occurred. try again later";
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
            return View(current);
        }
    }
}