﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgrammingForTheCloud63B_Website.Controllers
{
    public class CronController : Controller
    {
        // GET: Cron
        public ActionResult Index()
        {
            return View();
        }

        [ApiKeyAuthorize]
        public ActionResult RunEveryMinute()
        {
            if (HttpContext.Application["counter"] == null)
                HttpContext.Application["counter"] = 1;
            else
            {
                int counter = (int)HttpContext.Application["counter"];
                counter++;
            }

            return Content("Done");
        }

    //    [ApiKeyAuthorize]
        public ActionResult RunOnceADay()
        {
            ItemsRepository ir = new ItemsRepository();

            ir.MyConnection.Open();
            var itemsInDb = ir.GetItems();
            ir.MyConnection.Close();

            RedisRepository rr = new RedisRepository();
            if(rr.HasItemsInCacheChanged(itemsInDb.ToList()) == true)
            {
                rr.SetItemsInCache(itemsInDb.ToList());
                return Content("done");
            }
            else
                    return Content("Items were not updated since they are still the same");
        }
    }

    public class ApiKeyAuthorizeAttribute : AuthorizeAttribute
    {

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //HttpContext.Current.Request.Form["apikey"]   with google cloud Cloud Scheduler
            var apiKey = HttpContext.Current.Request.Headers["apikey"];
            if (apiKey != null)
            {
                try
                {
                    byte[] data = System.Convert.FromBase64String(apiKey);

                    string apiKeyOriginalValue = System.Text.ASCIIEncoding.ASCII.GetString(data);

                    if (apiKeyOriginalValue == "A88A7DF3-151E-45DC-B30A-2CFC1895846C")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }


        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    var apiKey = filterContext.RequestContext.HttpContext.Request.Headers["apikey"];
        //    if(apiKey != null)
        //    {
        //        try
        //        {
        //            byte[] data = System.Convert.FromBase64String(apiKey);

        //            string apiKeyOriginalValue = System.Text.ASCIIEncoding.ASCII.GetString(data);

        //            if (apiKeyOriginalValue == "ryanattard")
        //            {
        //                filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
        //            }
        //            else
        //            {
        //                filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
        //                base.OnAuthorization(filterContext);
        //            }
        //        }
        //        catch
        //        {
        //            filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
        //            base.OnAuthorization(filterContext);
        //        }
        //    }
        //    else
        //    {
        //        filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
        //        base.OnAuthorization(filterContext);
        //    }
        //}
    }

}