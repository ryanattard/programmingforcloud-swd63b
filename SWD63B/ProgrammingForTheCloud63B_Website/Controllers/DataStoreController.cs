﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Google.Cloud.Datastore.V1;

namespace ProgrammingForTheCloud63B_Website.Controllers
{
    public class DataStoreController : Controller
    {
        // GET: DataStore
        public ActionResult Index()
        {
            var dbStoreHandle = DatastoreDb.Create("progforcloud63at");
            Query q = new Query("Email");
            var listOfEmails = dbStoreHandle.RunQuery(q).Entities;
            List<Email> emailsToReturn = new List<Email>();
       //     List<dynamic> list = new List<dynamic>();
            foreach (var email in listOfEmails)
            {
       //         list.Add(new { to = email["to"] });
                Email e = new Email()
                {
                    to = email["to"].StringValue,
                    from = email["from"].StringValue,
                    subject = email["subject"].StringValue,
                    content = email["content"].StringValue,
                    id = email.Key.Path.First().Id
                };
                emailsToReturn.Add(e);
            }

            return View(emailsToReturn);
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Email email)
        {
            var dbStoreHandle = DatastoreDb.Create("progforcloud63at");

            //create a table/ kind in the datastore
            KeyFactory keyFactory = dbStoreHandle.CreateKeyFactory("Email");

            //create a new row in the newly created kind/table
            var pk = keyFactory.CreateIncompleteKey();

            //{to: xxx, from: xxx, subject: xxxx, content: xxxx, Path: [{id:23423412341234}]}

            Entity myNewRow = new Entity()
            {
                ["to"] = email.to,
                ["from"] = email.from,
                ["subject"] = email.subject,
                ["content"] = email.content,
                Key = pk
            };

            dbStoreHandle.Insert(myNewRow);

            return RedirectToAction("Index");
        }

        public ActionResult DeleteEmail(long id)
        {
            var dbStoreHandle = DatastoreDb.Create("progforcloud63at");

            //create a table/ kind in the datastore
            KeyFactory keyFactory = dbStoreHandle.CreateKeyFactory("Email");

            using (var transaction = dbStoreHandle.BeginTransaction())
            {
                Entity emailToDelete = dbStoreHandle.Lookup(keyFactory.CreateKey(id));
                if(emailToDelete != null)
                {
                    transaction.Delete(emailToDelete);
                }
                //more items to delete...
                transaction.Commit();
            }

            return RedirectToAction("Index");
        }


        public ActionResult Details(long id)
        {
            var dbStoreHandle = DatastoreDb.Create("progforcloud63at");
            KeyFactory keyFactory = dbStoreHandle.CreateKeyFactory("Email");

            Query q = new Query("Email");
            q.Filter = Google.Cloud.Datastore.V1.Filter.Equal("__key__", keyFactory.CreateKey(id));

            var listOfEmails = dbStoreHandle.RunQuery(q).Entities;

            Email e = new Email();
            if(listOfEmails != null )
            {
                if(listOfEmails.Count > 0)
                {
                    e.to = listOfEmails.First()["to"].StringValue;
                    e.from = listOfEmails.First()["from"].StringValue;
                    e.subject = listOfEmails.First()["subject"].StringValue;
                    e.content = listOfEmails.First()["content"].StringValue;
                    e.id = listOfEmails.First().Key.Path.First().Id;
                }
            }

            return View(e);
        }







    }

    public class Email
    {
        public long id { get; set; }
        public string to { get; set; }
        public string from { get; set; }
        public string subject { get; set; }
        public string content { get; set; }
    }
}