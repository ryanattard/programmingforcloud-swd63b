﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgrammingForTheCloud63B_Website.Controllers
{
    public class PubsubController : Controller
    {
        // GET: Pubsub
        public ActionResult Index()
        {

            PubSubRepository psr = new PubSubRepository();

            var t = psr.GetOrCreateTopic("SWD63BTestMessageQueue");

            //in assignment: call this method when user submits interest in a property
            psr.PublishMessageToTopic("hello world", t);



            //in assignment call this method from Console App and consequently send the message retrieved as an email
           string content= psr.ReadMessageFromTopic("SWD63bTestSubscription", t);


            return Content(content);
        }
    }
}