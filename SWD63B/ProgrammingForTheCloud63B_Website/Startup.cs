﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProgrammingForTheCloud63B_Website.Startup))]
namespace ProgrammingForTheCloud63B_Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
