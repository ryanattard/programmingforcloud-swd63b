﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ProgrammingForTheCloud63B_Website
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            ItemsRepository ir = new ItemsRepository();
            
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

            new RedisRepository().SetItemsInCache(ir.GetItems().ToList());
            }
            catch (Exception ex)
            {
                //log exception
             
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
        }
    }
}
