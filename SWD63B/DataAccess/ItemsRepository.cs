﻿using Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ItemsRepository: ConnectionClass
    {
        public ItemsRepository():base()
        { }


        public IQueryable<Item> GetItems( )
        {
            //DataReader

            //if (MyConnection.State == System.Data.ConnectionState.Closed)
            //  MyConnection.Open();

            string sql = "Select Items.id, name, price, category_fk, categories.title, imagepath From Items ";
            sql += "inner join Categories on Items.Category_Fk = Categories.Id  ";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
          


            List<Item> mYList = new List<Item>();
            using (NpgsqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    mYList.Add(
                        new Item()
                        {
                            Id = Convert.ToInt16(reader["id"]),
                            Name = reader.GetString(1),
                            Price = reader.GetDecimal(2),
                            Category_Fk = reader.GetInt32(3),
                            ItemCategory = new Category() { Id = reader.GetInt32(3), Title = reader.GetString(4) }
                            ,
                            ImagePath = string.IsNullOrEmpty(reader[5].ToString()) ? "" : reader.GetString(5)

                        });
                }
            }

            return mYList.AsQueryable();

        }


        public IQueryable<Item> GetItems(int category)
        {
            //DataReader

            //if (MyConnection.State == System.Data.ConnectionState.Closed)
            //  MyConnection.Open();

            string sql = "Select Items.id, name, price, category_fk, categories.title From Items ";
                sql += "inner join Categories on Items.Category_Fk = Categories.Id where category_fk = @category";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@category", category);


            List<Item> mYList = new List<Item>();
            using (NpgsqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    mYList.Add(
                        new Item()
                        {
                            Id = Convert.ToInt16(reader["id"]),
                            Name = reader.GetString(1),
                            Price = reader.GetDecimal(2),
                            Category_Fk = reader.GetInt32(3),
                            ItemCategory = new Category() { Id=reader.GetInt32(3), Title = reader.GetString(4)}
                        });
                }
            }

            return mYList.AsQueryable();

        }


        public Item GetItem(int id)
        {
            string sql = "Select Items.id, name, price, category_fk, categories.title From Items ";
            sql += "inner join Categories on Items.Category_Fk = Categories.Id where Items.Id = @id ";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);

            cmd.Parameters.AddWithValue("@id", id);

            Item myItem = new Item();
            using (NpgsqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    myItem =
                        new Item()
                        {
                            Id = Convert.ToInt16(reader["id"]),
                            Name = reader.GetString(1),
                            Price = reader.GetDecimal(2),
                            Category_Fk = reader.GetInt32(3),
                            ItemCategory = new Category() { Id = reader.GetInt32(3), Title = reader.GetString(4) }
                        };
                }
            }

            return myItem;
        }

        public string GetItemCategory(int id)
        {
            string sql = "Select Title from Categories where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            string categoryTitle = (string)cmd.ExecuteScalar();

            return categoryTitle;
        }

        public void AddItem(Item i)
        {
            string sql = "Insert into Items (name, price, category_fk, imagepath) Values (@name, @price, @categoryId, @image)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@name", i.Name);
            cmd.Parameters.AddWithValue("@price", i.Price);
            cmd.Parameters.AddWithValue("@categoryId", i.Category_Fk);
            cmd.Parameters.AddWithValue("@image", i.ImagePath);
            cmd.ExecuteNonQuery();
        }

        public void DeleteItem(int id)
        {
            string sql = "delete from items where id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Transaction = MyTransaction; //to make the command participate in an opened transaction


            cmd.ExecuteNonQuery(); //executenonquery >>> Insert, Update, Delete
        }

        public void UpdateItem(Item current)
        {
            string sql = "Update Items Set Name = @name, Price = @price, Category_Fk = @category where Id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", current.Id);
            cmd.Parameters.AddWithValue("@name", current.Name);
            cmd.Parameters.AddWithValue("@price", current.Price);
            cmd.Parameters.AddWithValue("@category", current.Category_Fk);

            cmd.ExecuteNonQuery();
        }
    }
}
