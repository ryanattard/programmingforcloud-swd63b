﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Npgsql;
 

namespace DataAccess
{
    public class ConnectionClass
    {
        public NpgsqlConnection  MyConnection { get; set; }
        public NpgsqlTransaction MyTransaction { get; set; }



        public ConnectionClass()
        {
            //1. Add NpgSql NuGet Package
            //2. Add Reference System.Configuration
            //3. Add reference System.Web
            //Server=35.229.114.135;Port=5432;Database=postgres;User Id=postgres;Password=Mcast123; 

            string str = WebConfigurationManager.ConnectionStrings["postgresConnection"].ConnectionString;
            MyConnection = new NpgsqlConnection(str);
        }
    }
}
