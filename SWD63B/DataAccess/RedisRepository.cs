﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StackExchange.Redis;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace DataAccess
{
    public class RedisRepository
    {

        ConnectionMultiplexer cacheManager;
        public RedisRepository()
        {
            
            cacheManager = ConnectionMultiplexer.Connect(
                "redis-14991.c1.us-east1-2.gce.cloud.redislabs.com:14991,password=Kf9m4fNaKcHxRMKAU1mFLKGuPgNJsorM");
            
           
        }

        public List<Item> LoadItemsFromCache()
        {
            var db = cacheManager.GetDatabase();
            string items = db.StringGet("items");
            if(string.IsNullOrEmpty(items) == true)
            {
                return null;
            }
            else
            {
                return JsonConvert.DeserializeObject<List<Item>>(items);
            }

        }

        public void SetItemsInCache(List<Item> items)
        {
            var db = cacheManager.GetDatabase();
            if(items != null)
            db.StringSet("items", JsonConvert.SerializeObject(items));
        }

        public bool HasItemsInCacheChanged(List<Item> items)
        {
            string digest1 = HashValue(JsonConvert.SerializeObject(items));
            var dataInCache = LoadItemsFromCache();
            if (dataInCache == null) return false;
            string digest2 = HashValue(JsonConvert.SerializeObject(dataInCache));
            return !(digest1 == digest2);
        }

        private string HashValue(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputAsBytes = Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(md5.ComputeHash(inputAsBytes));
        }

         


    }
}
