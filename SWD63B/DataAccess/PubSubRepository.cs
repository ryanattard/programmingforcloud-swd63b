﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;
using Newtonsoft.Json;

namespace DataAccess
{
    public class PubSubRepository
    {
        public Topic GetOrCreateTopic(string name)
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();
            Topic t = null;

            try
            {
                t = client.GetTopic(new TopicName("progforcloud63at", name));
            }
            catch (RpcException e)
            {
                if (e.Status.StatusCode == StatusCode.NotFound)
                {
                    t = client.CreateTopic(new TopicName("progforcloud63at", name));
                }
            }

            return t;
        }

        public void PublishMessageToTopic(string msg, Topic t)
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();
           

            List<PubsubMessage> myMessages = new List<PubsubMessage>();

//            string jsonStr = JsonConvert.SerializeObject(msg);

            myMessages.Add(new PubsubMessage()
            {
                Data = ByteString.CopyFromUtf8(msg) 
            });

            client.Publish(t.TopicName, myMessages);
        }


        //methods used to read and pull messages from pub/sub
        //methods that will need to be called from the Console App

        public Subscription GetOrCreateSubscription(string subscriptionName, Topic t)
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();
            Subscription s = null;

            try
            {
                s = client.GetSubscription(new SubscriptionName("progforcloud63at", subscriptionName));
            }
            catch (RpcException e)
            {
                if (e.Status.StatusCode == StatusCode.NotFound)
                {
                    s = client.CreateSubscription(
                        new SubscriptionName("progforcloud63at", subscriptionName), 
                        t.TopicName, null, 60);
                }
            }

            return s;
        }


        public string ReadMessageFromTopic(string subscriptionName, Topic t)
        {

            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();

            var s = GetOrCreateSubscription(subscriptionName, t);

            var pullResponse = client.Pull(s.SubscriptionName, true, 1);

            var message = pullResponse.ReceivedMessages.FirstOrDefault();
            if (message == null) return "";
            else
            {
                string actualMessageContent = message.Message.Data.ToStringUtf8();

//                MailMessage mm = JsonConvert.DeserializeObject<MailMessage>(actualMessageContent);

                List<string> ackIds = new List<string>() { message.AckId };
                client.Acknowledge(s.SubscriptionName, ackIds);
                return actualMessageContent;
            }


        }


    }
}
